import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  url = 'http://localhost:8000/api';
  httpOptions = {
    headers: new HttpHeaders({        
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept':'application/json',
    })
  };

  constructor(private http: HttpClient) { }
  
    
  getCourses():  Observable<any>{
    return this.http.get(this.url+'/courses', this.httpOptions);
  }
  
  getCourse(course): Observable<any>{
    console.log(course);
    return this.http.get(this.url+'/courses',this.httpOptions);
  }

  setCourse(course):  Observable<any>{
    console.log(course);
    return this.http.post(this.url+'/courses/'+course, this.httpOptions);
  }

  deleteCourse(course): Observable<any> {
    console.log(course);
    return this.http.delete(this.url+'/courses/'+course, this.httpOptions);
  }

}
